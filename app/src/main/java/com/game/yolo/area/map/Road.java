package com.game.yolo.area.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.game.yolo.area.R;
import com.game.yolo.area.others.MyButton;

import java.util.ArrayList;

/**
 * Created by yolo on 8/9/14.
 */
public class Road {
    public Road() {
        nodes = new ArrayList<Node>();
    }

    public void setDimension(int width, int height) {
        this.width = width / 10;
        this.height = height / 10;

        Node node0 = new Node(0, 0, 9 * height / 10);
        Node node1 = new Node(1, 32, 9 * height / 10);
        Node node2 = new Node(2, 64, 9 * height / 10);

        Node node3 = new Node(3, 0, 0);
        Node node4 = new Node(4, 32, 0);
        Node node5 = new Node(5, 64, 0);

        node0.addAdj(node3);
        node1.addAdj(node0);
        node1.addAdj(node4);
        node4.addAdj(node1);
        node2.addAdj(node5);
        node5.addAdj(node2);

        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(20);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    public void onDraw(Canvas canvas) {
        for (Node node : nodes) {
            ArrayList<Node> temp = node.getAdj();
            for (Node nd : temp)
                canvas.drawLine(node.posX, node.posY, nd.posX, nd.posY, drawPaint);
        }
    }

    /** A/D
     * nodes    : Different crossroads of the road
     * width    : Width of the screen
     * height   : Height of the screen
     * start    : The start of the road
     * end      : The end of the road
     */
    ArrayList<Node> nodes;
    int width;
    int height;

    MyPoint start;
    MyPoint end;

    private Paint drawPaint;
    private int paintColor = 0xFF660000;
}
