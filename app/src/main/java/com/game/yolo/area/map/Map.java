package com.game.yolo.area.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.area.R;

/**
 * Created by yolo on 8/9/14.
 */
public class Map extends View {
    public Map(Context context, int width, int height) {
        super(context);

        this.icon = prepareBitmap(getResources().getDrawable(R.drawable.tile_grass), iconSize, iconSize);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        System.out.println("COORD " + width + "," + height);
        for (int i = 0; i < width; i += iconSize) {
            for (int j = 0; j < height; j += iconSize) {
                canvas.drawBitmap(icon, i, j, null);
            }
        }
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    /** A/D
     * icon     : The sprite of the tile
     * sizeIcon : The size of the sprite
     * width    : The width of the screen
     * height   : The height of the screen
     */

    private int iconSize = 32;
    private Bitmap icon;

    private int width;
    private int height;
}
