package com.game.yolo.area.others;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.area.R;

import java.util.ArrayList;

/**
 * Created by yolo on 8/9/14.
 */
public class MyButton extends View {
    public MyButton(Context context, int icon_id, float posX, float posY, int width, int height) {
        super(context);

        this.icon = prepareBitmap(getResources().getDrawable(icon_id), width, height);
        //this.icon = rotateBitmap(this.icon, 5, width, height);
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
    }

    /*public Bitmap rotateBitmap(Bitmap source, float angle, int width, int height)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, this.posX + this.posX / 2, this.posY + this.posY / 2);
        return Bitmap.createBitmap(source, 0, 0, width, height, matrix, true);
    }*/

    @Override
     public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        System.out.println("Coord of the button : " + posX + "," + posY);
        canvas.drawBitmap(icon, posX, posY, null);
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    /** A/D
     * posX         : Position on X axis of the button
     * posY         : Position on Y axis of the button
     * width        : Width of the interface
     * height       : Height of the interface
     */

    private float posX;
    private float posY;
    private float width;
    private float height;
    private Bitmap icon;
}
