package com.game.yolo.area.map;

/**
 * Created by yolo on 8/10/14.
 */
public class MyPoint {
    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    /** A/D
     * x    : The coordinate on X axis
     * y    : The coordinate on Y axis
     */

    int x;
    int y;
}
