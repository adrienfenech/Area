package com.game.yolo.area.map;

import java.util.ArrayList;

/**
 * Created by yolo on 8/9/14.
 */
public class Node {
    public Node(int id, float posX, float posY) {
        this.id = id;
        adj = new ArrayList<Node>();
        this.posX = posX;
        this.posY = posY;
    }

    public void addAdj(Node node) {
        adj.add(node);
    }
    public ArrayList<Node> getAdj() { return adj; }

    /** A/D
     * id   : Identifiant of the node
     * adj  : Nodes adj
     * posX     : Position on X axis of the node
     * posY     : Position on Y axis of the node
     */
    int id;
    ArrayList<Node> adj;
    float posX;
    float posY;
}
