package com.game.yolo.area.others;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.area.R;

import java.util.ArrayList;

/**
 * Created by yolo on 8/9/14.
 */
public class Interface extends View {
    public Interface(Context context) {
        super(context);
        this.context = context;
        buttonList = new ArrayList<MyButton>();
    }

    public void setDimension(int width, int height) {
        this.width = width / 10;
        this.height = height / 10;

        buttonSize = height / 10;
        this.icon = prepareBitmap(getResources().getDrawable(R.drawable.color_gray), width, height);
        buttonList.add(new MyButton(context, R.drawable.connection, width / 5 - buttonSize / 2, 9 * this.height, buttonSize, buttonSize));
        buttonList.add(new MyButton(context, R.drawable.construct, 2 * width / 5 - buttonSize / 2, 9 * this.height, buttonSize, buttonSize));
        buttonList.add(new MyButton(context, R.drawable.grow, 3 * width / 5 - buttonSize / 2, 9 * this.height, buttonSize, buttonSize));
        buttonList.add(new MyButton(context, R.drawable.destruct, 4 * width / 5 - buttonSize / 2, 9 * this.height, buttonSize, buttonSize));
    }

    @Override

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        System.out.println("START");
        canvas.drawBitmap(icon, 0, 9 * height, null);
        for (MyButton button : buttonList) {
            button.onDraw(canvas);
        }
        System.out.println("STOP");
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    /** A/D
     * width        : Width of the interface
     * height       : Height of the interface
     * buttonSize   : Size of a button
     * buttonList   : List of the interface's button
     */

    private float width;
    private float height;
    private int buttonSize;
    private Bitmap icon;
    protected Context context;
    ArrayList<MyButton> buttonList;
}
