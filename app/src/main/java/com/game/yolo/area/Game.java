package com.game.yolo.area;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

import com.game.yolo.area.map.Map;
import com.game.yolo.area.map.Road;
import com.game.yolo.area.others.Interface;
import com.game.yolo.area.vehicle.Car;
import com.game.yolo.area.vehicle.Vehicle;

/**
 * Created by yolo on 8/1/14.
 */
public class Game extends View {

    public Game(Context context) {
        super(context);
        map = new Map(context, width, height);
        inter = new Interface(context);
        //car = new Car(context, 10);
        road = new Road();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!firstDrawn) {
            map.onDraw(canvas);
            road.onDraw(canvas);
            //
            // car.onDraw(canvas);
            inter.onDraw(canvas);
        }
        else {
            System.out.println("DIMENSION " + getWidth() + "," + getHeight());
            width = getWidth();
            height = getHeight();
            firstDrawn = false;
            deployDimension();
            invalidate();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float x = event.getX();
        final float y = event.getY();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (y > 9 * height / 10) {
                    if (x > width / 5 - buttonSize / 2 && x < width / 5 + buttonSize / 2) {
                        // Action When Road is Selected
                        
                    }
                    else if (x > 2 * width / 5 - buttonSize / 2 && x < 2 * width / 5 + buttonSize / 2) {
                        // Action When Construction is Selected
                    }
                    else if (x > 3 * width / 5 - buttonSize / 2 && x < 3 * width / 5 + buttonSize / 2) {
                        // Action When Decoration is Selected
                    }
                    else if (x > 4 * width / 5 - buttonSize / 2 && x < 4 * width / 5 + buttonSize / 2) {
                        // Action When Destruction is Selected
                    }
                }
                /*else if (x > car.getPosX()
                        && x < car.getPosX() + car.getIconSize()
                        && y > car.getPosY()
                        && y < car.getPosY() + car.getIconSize()) {
                    car.setDeltaX(x - car.getPosX());
                    car.setDeltaY(y - car.getPosY());
                    car.setPrevX(car.getPosX());
                    car.setPrevY(car.getPosY());

                    return true;
                }*/
                break;
            case MotionEvent.ACTION_MOVE:
                car.setPrevX(car.getPosX());
                car.setPrevY(car.getPosY());
                car.setPosX(x - car.getDeltaX());
                car.setPosY(y - car.getDeltaY());
                partialInvalidate(car);
                //invalidate();
                return true;
            case MotionEvent.ACTION_UP:
                return true;
        }
        return super.onTouchEvent(event);
    }

    private void partialInvalidate(Car car) {
        final int iconSize = car.getIconSize();
        final float posX = car.getPosX();
        final float posY = car.getPosY();
        final float prevX = car.getPrevX();
        final float prevY = car.getPrevY();

        final int minX = (int)Math.min(posX, prevX);
        final int minY = (int)Math.min(posY, prevY);
        final int maxX = (int)Math.max(posX + iconSize, prevX + iconSize);
        final int maxY = (int)Math.max(posY + iconSize, prevY + iconSize);

        invalidate(minX, minY, maxX, maxY);
    }

    private void deployDimension() {
        map.setDimension(width, height);
        inter.setDimension(width, height);
        road.setDimension(width, height);
        buttonSize = height / 10;
    }

    /** A/D
     * inter        : Interface of the game (buttons, ...)
     * map          : Game map
     * firstDrawn   : Use to get dimension at the first draw
     * width        : Width of the screen
     * height       : Height of the screen
     */

    Car car;
    Map map;
    Road road;
    Interface inter;
    int width;
    int height;
    boolean firstDrawn = true;
    int buttonSize;
}
