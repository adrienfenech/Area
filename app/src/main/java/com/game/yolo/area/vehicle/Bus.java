package com.game.yolo.area.vehicle;
import android.content.Context;
import com.game.yolo.area.R;

/**
 * Created by yolo on 8/8/14.
 */
public class Bus extends Vehicle {
    public Bus(Context context, int money) {
        super(context, money);

        this.icon = prepareBitmap(getResources().getDrawable(R.drawable.bus), iconSize, iconSize);
    }
}
