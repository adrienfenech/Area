package com.game.yolo.area.vehicle;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.content.Context;


/**
 * Created by yolo on 8/1/14.
 */
public abstract class Vehicle extends View {
    public Vehicle(Context context, int money) {
        super(context);
        this.money = money;
        resetView();
    }

    @Override
    public void onDraw(Canvas canvas) {
       super.onDraw(canvas);

        canvas.drawBitmap(icon, posX, posY, null);
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    protected void resetView() {
        posX = this.getWidth() / 2;
        posY = this.getHeight() / 2;
    }

    /** Get / Set
     * posX / posY
     * prevX / prevY
     * deltaX / deltaY
     * sizeIcon
     */
    public float getPosX() { return posX; }
    public void setPosX(float posX) { this.posX = posX; }

    public float getPosY() { return posY; }
    public void setPosY(float posY) { this.posY = posY; }

    public float getPrevX() { return prevX; }
    public void setPrevX(float prevX) { this.prevX = prevX; }

    public float getPrevY() { return prevY; }
    public void setPrevY(float prevY) { this.prevY = prevY; }

    public float getDeltaX() { return deltaX; }
    public void setDeltaX(float deltaX) { this.deltaX = deltaX; }

    public float getDeltaY() { return deltaY; }
    public void setDeltaY(float deltaY) { this.deltaY = deltaY; }

    public int getIconSize() { return iconSize; }

    /** A/D
     * icon     : The sprite of the vehicle
     * sizeIcon : The size of the sprite
     * money    : The money which the vehicle will spend on the area
     * posX     : Position on X axis of the vehicle
     * posY     : Position on Y axis of the vehicle
     * prevX    : Previous position on X axis of the vehicle
     * prevY    : Previous position on Y axis of the vehicle
     * deltaX   : Difference between the X position pressed and the X position of the sprite
     * deltaY   : Difference between the Y position pressed and the Y position of the sprite
     */
    protected Bitmap icon;
    protected int iconSize = 30;
    protected int money = 1;

    protected float posX;
    protected float posY;
    protected float prevX;
    protected float prevY;

    protected float deltaX;
    protected float deltaY;
}
